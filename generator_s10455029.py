source_file , clone_file , reportfile = input().split()

import shutil
import itertools
import random
import keyword
import json

################處理迴圈形式的執行列#####################
def execute(m):
    if str(m)[:4]=="for " or str(m)[:6]=="while " or str(m)[:3]=="if " or str(m)[:4]=="else" \
           or str(m)[:6]=="class " or str(m)[:4]=="try " or str(m)[:7]=="except "or str(m)[:8]=="finally "\
           or str(m)[:5]=="elif " or str(m)[:5]=="with " :
        return True
    else:
        return False
################檔案匯入後基本處理######################
def inputfile(source_file):
    file = open(source_file, "r",encoding="utf-8")
    lines = file.readlines()

    temp_list = []

    lines = [w.replace('\t','    ') for w in lines]

    for n in lines:
        if str(n)[:1] ==" " or str(n)[:4]=="def "  or execute(str(n)) is True:
            temp_list.append(n)    

    lines = temp_list
    lines = remove_execute_lines(lines)

    return lines,temp_list

####################################################    
def remove_execute_lines(lines):
    execute_position = []
    next_def=0
    temp_list = []

    for m in lines:
        if execute(str(m)) is True:
            execute_position.append(lines.index(m))

    if len(execute_position)>0:            
        for i in range(int(execute_position[0]),len(lines)):
            a = lines[i]
            if a[:4]=="def ":
                next_def = i

        if next_def != 0:
            if len(execute_position)>1:
                if execute_position[1]>next_def:
                    for i in range(next_def,execute_position[1]):
                        temp_list.append(lines[i])
                else:
                    for i in range(next_def,len(lines)):
                        temp_list.append(lines[i])
                    
            else:
                for i in range(next_def,len(lines)):
                    temp_list.append(lines[i])
                                   
        else:
            for i in range(0,execute_position[0]):
                temp_list.append(lines[i])
    else:
        temp_list = lines

    return temp_list
####################################################
def type1(source_file):
    
    lines,temp_list = inputfile(source_file)

    total_def,main_fun = count_def(lines)

    if total_def >1 and main_fun==0:
        lines = no_main(lines)
    if total_def >1 and main_fun>0 and ignore_main(lines) is False:
        lines = delete_main(lines)
    if total_def >1 and main_fun>0 and ignore_main(lines) is True:
        lines = no_main(lines)

    startline,endline = check_num(lines,source_file)

    if len(lines)<3:
        more_than2 = 0
        lines = add_line(lines)
        temp_string = "".join(itertools.chain(*lines))
        temp_split = temp_string.split("\n")
    else:
        more_than2 = 1
        temp_string = "".join(itertools.chain(*lines))
        temp_split = temp_string.split("\n")
        
        random_position = random.sample(range(1,len(temp_split)-2),1)
        temp_split[random_position[0]] = temp_split[random_position[0]]+"#HAHAHAHA"
    
    temp_string2 = "\n".join(temp_split)

    with open(clone_file, 'a',encoding="utf-8") as f:
        f.write("\n"+ temp_string2)

    startline_clone,endline_clone = check_num(lines,clone_file)

    return startline,endline,startline_clone,endline_clone,more_than2
####################################################################
def type1_2(source_file):

    lines = delete_first(source_file)

    startline,endline = check_num(lines,source_file)

    if len(lines)<3:        
        lines = add_line(lines)
        temp_string = "".join(itertools.chain(*lines))
        temp_split = temp_string.split("\n")
    else:
        temp_string = "".join(itertools.chain(*lines))
        temp_split = temp_string.split("\n")
        
        random_position = random.sample(range(1,len(temp_split)-2),1)
        temp_split[random_position[0]] = temp_split[random_position[0]]+"#HAHAHAHA"
    
    temp_string2 = "\n".join(temp_split)

    with open(clone_file, 'a',encoding="utf-8") as f:
        f.write("\n"+ temp_string2)

    startline_clone,endline_clone = check_num(lines,clone_file)

    return startline,endline,startline_clone,endline_clone
####################################################################
def choose_def(lines):
    
    temp_list = []
    count_def = 0
    
    for m in lines:
        if str(m)[:4]=="def " and str(m)[:9]!="def main(":
            count_def+=1
        if count_def==2:
            temp_list.append(m)

    return temp_list

def delete_first(source_file):
    
    lines,temp_list = inputfile(source_file)
    total_def,main_fun = count_def(lines)
    lines = choose_def(lines)
    return lines
####################################################################        
def type2(source_file):
    
    lines,temp_list = inputfile(source_file)

    total_def,main_fun = count_def(lines)

    if total_def >1 and main_fun==0:
        lines = no_main(lines)
    if total_def >1 and main_fun>0 and ignore_main(lines) is False:
        lines = delete_main(lines)
    if total_def >1 and main_fun>0 and ignore_main(lines) is True:
        lines = no_main(lines)

    startline,endline = check_num(lines,source_file)

    temp_string = "".join(itertools.chain(*lines))

    symbol = ["+","-","/","(",")",":",".",",","'",'"',"<",">","=","*","%","[","]"]
    saperate_list = []
    
    for ch in symbol:
        symbol_list = [i for i, x in enumerate(temp_string) if x == ch]
        for i in symbol_list:
            saperate_list.append(i)
            
    saperate_list = sorted(saperate_list)
    saperate_string = list(temp_string)

    for i in saperate_list:
        saperate_string[i] = " "+ saperate_string[i] +" "

    new = ''.join(saperate_string)
    for ch in ["-","*"]:
        if ch in new:
            new = new.replace(ch," "+ch+" ")

    for ch in ["\n"]:
        if ch in new:
            new = new.replace(ch," "+ch)

    #####判斷是否復合運算子#####
    operator = ["+","-",'*',"/","=","<",">","!","*","^"]
    saperate_list2 = []
    
    for ch in operator:
        symbol_list2 = [i for i, x in enumerate(new) if x == ch]

        for i in symbol_list2:
            saperate_list2.append(i)
            
    saperate_list2 = sorted(saperate_list2)
    is_multi_operator = []

    for i in saperate_list2:
        next_char = i+3

        if next_char in saperate_list2:
            is_multi_operator.append(i)

    new2 = list(new)
    
    for j in is_multi_operator:
        new2[j] = new2[j] + new2[j+3]

    for i in range(len(is_multi_operator)):
        is_multi_operator[i] = is_multi_operator[i]+3

    new3 = []
    for char in range(len(new2)):
        if char not in is_multi_operator:
            new3.append(new2[char])

    new3 = ''.join(new3)

    temp_split = new3.split(" ")  

    templist = []

    for x in temp_split:
        if temp_split.count(x) > 1 and x!='"' and x!="" and x.isdigit() is False and keyword.iskeyword(x) is False \
           and compare_with_builtin_function(x) is False and isOperator(x) is False and x!="\n" and x!="[" and x!="]":
            templist.append(x)

    if len(templist)==0:
        print("There is no identifier can be changed to type2!!!")

    random_clone = random.sample(range(len(templist)),1)[0]
    clone_item = templist[random_clone]

    indices = [i for i, x in enumerate(temp_split) if x == clone_item]

    for i in range(len(indices)):
        temp_split[indices[i]] = temp_split[indices[i]]+"_clone"

    ####處理**的問題####
    double_production = []
    for a in range(len(temp_split)-4):
        if temp_split[a]=="*" and temp_split[a+2]=="" and temp_split[a+4]=="*":
            double_production.append(a+1)
            double_production.append(a+2)

    temp_split2 = []
    for b in range(len(temp_split)):
        if b in double_production:
            continue
        else:
            temp_split2.append(temp_split[b])

    temp_split = temp_split2
    #################
    
    final = " ".join(temp_split)

    for ch in [" += "," : "," > "," < "," >= "," <= "," != "," == "," ++ "," -= "," --"," . "," , "," % "," // "," * "," ** "," ( "," ) "," [ "," ] "," ! "]:
        if ch in final:
            ch_trim = ch.strip()
            final = final.replace(ch,ch_trim)

    with open(clone_file, 'a',encoding="utf-8") as f:
        f.write("\n"+ final)

    return startline,endline        
########################################################
def type3(source_file):
    
    lines,temp_list = inputfile(source_file)

    total_def,main_fun = count_def(lines)

    if total_def >1 and main_fun==0:
        lines = no_main(lines)
    if total_def >1 and main_fun>0 and ignore_main(lines) is False:
        lines = delete_main(lines)
    if total_def >1 and main_fun>0 and ignore_main(lines) is True:
        lines = no_main(lines)

    startline,endline = check_num(lines,source_file)

    temp_string = "".join(itertools.chain(*lines))
    temp_split = temp_string.split("\n")
    
    temp_string2 = "\n".join(temp_split)

    temp_split[0] = temp_split[0]+"\n    print('Add this for Type3!')"
    temp_string2 = "\n".join(temp_split)

    with open(clone_file, 'a',encoding="utf-8") as f:
        f.write("\n"+ temp_string2)

    return startline,endline
#########################################################
def type3_2(source_file):
    
    lines = delete_first(source_file)

    startline,endline = check_num(lines,source_file)

    temp_string = "".join(itertools.chain(*lines))
    temp_split = temp_string.split("\n")
    
    temp_string2 = "\n".join(temp_split)

    temp_split[0] = temp_split[0]+"\n    print('Add this for Type3!')"
    temp_string2 = "\n".join(temp_split)

    with open(clone_file, 'a',encoding="utf-8") as f:
        f.write("\n"+ temp_string2)

    return startline,endline

#####檢查是否為內建函數#####
def compare_with_builtin_function(x):
    builtin_function = ["abs","apply","all","any","basestring","bool","callable","chr","classmethod","cmp","complex","delattr","dict","dir","divmod","enumerate","eval"
                        ,"execfile","file","filter","float","frozenset","getattr","globals","hasattr","help","id","input","int","isinstance","issubclass","iter","len"
                        ,"list","locals","long","map","max","min","object","oct","open","ord","pow","property","range","raw_input","reduce","reload","repr","reversed"
                        ,"round","set","setattr","sorted","staticmethod","str","sum","super","tuple","type","unichr","unicode","vars","xrange","zip"]

    if x in builtin_function:
        return True
    else:
        return False
#######檢查是否是符號#######
def isOperator(x):
    operator = ["+=","=","\n",":",">","<",">=","<=","!=","==","++","-=","--",".",",","%","/","//","*","+","-","**","(",")"]

    if x in operator:
        return True
    else:
        return False

####################若程式碼中沒有main函數#############################    
def no_main(lines):
    
    temp_list = []
    count_def = 0

    for m in lines:
        if str(m)[:4]=="def " and str(m)[:9]!="def main(":
            count_def+=1
        if count_def<2:
            temp_list.append(m)

    return temp_list
################計算有幾個def,如果超過一個,找出是否有def main#################
def count_def(lines):

    total_def = 0
    main_fun = 0
    for x in lines:
        if str(x)[:4]=="def ":
            total_def +=1
            if str(x)[:9]=="def main(":
                main_fun+=1

    return total_def,main_fun
########################若有main且寫在最前面###########################
def delete_main(lines):
    temp_list = []
    end_of_main = 0

    for n in lines:
        if str(n)[:4]=="def " and str(n)[:9]!="def main(":
            end_of_main = lines.index(n)
            break
        
    a = len(lines)
    for i in range(end_of_main,a):
        temp_list.append(lines[i])

    temp_list = no_main(temp_list)

    return temp_list
#######################若有main不是寫在最前面###########################
def ignore_main(lines):
    first_def = 0
    main_def = 0
    for n in lines:
        if str(n)[:4]=="def " and str(n)[:9]!="def main(":
            first_def = lines.index(n)
            break

    for m in lines:
        if str(m)[:4]=="def "and str(m)[:9]=="def main(":
            main_def = lines.index(m)
            break

    if first_def<main_def: #回傳true當main不是第一個函數
        return True
    else:
        return False              
######################找出在source code中的行數##########################
def check_num(temp_list,source_file):

    file = open(source_file, "r",encoding="utf-8")
    check_linenum = file.readlines()

    check_linenum = [w.replace('\t','    ') for w in check_linenum]
    
    startline = 1
    endline = 1
    a = len(temp_list)-1

    for i in check_linenum:
        if temp_list[0] != i :
            startline += 1
        else:            
            break

    for i in check_linenum:
        if temp_list[a] != i :
            endline += 1            
        else:            
            break
    return startline,endline
#######################處理當函數不到3行時的情況#########################
def add_line(lines):
    lines.insert(1,'#HAHAHAHA\n')
    return lines

def more_than2_2(source_file):    
    lines,temp_list = inputfile(source_file)    
    total_def,main_fun = count_def(lines)
    lines = delete_first(source_file)
    if len(lines)<3:
        return True
    else:
        return False    
###################################################################
def main(source_file,clone_file):
    
    file = open(source_file, "r",encoding="utf-8")
    lines = file.readlines()

    lines = [w.replace('\t','    ') for w in lines]

    original = "".join(lines)

    total_def,main_fun = count_def(lines)

    f_start_line1,f_end_line1,startline_clone1,endline_clone1,more_than2 = type1(source_file)
    delta = (endline_clone1 - startline_clone1)+1

    if total_def<3:
        if more_than2 == 0:
            f_start_line2,f_end_line2 = type2(source_file)
            startline_clone2,endline_clone2 =  startline_clone1+delta+1 , endline_clone1+delta
            
            f_start_line3,f_end_line3 = type3(source_file)
            startline_clone3,endline_clone3 =  startline_clone2+delta , endline_clone2+delta+1

            f_start_line4,f_end_line4,startline_clone4,endline_clone4,a = type1(source_file)
            startline_clone4,endline_clone4 = endline_clone3+startline_clone4,endline_clone3+delta+1
            
            f_start_line5,f_end_line5 = type3(source_file)
            startline_clone5,endline_clone5 =  startline_clone4+delta+1 , endline_clone4+delta+1
        else:
            f_start_line2,f_end_line2 = type2(source_file)
            startline_clone2,endline_clone2 =  startline_clone1+delta+1 , endline_clone1+delta+1
            
            f_start_line3,f_end_line3 = type3(source_file)
            startline_clone3,endline_clone3 =  startline_clone2+delta+1 , endline_clone2+delta+1+1

            f_start_line4,f_end_line4,startline_clone4,endline_clone4,a = type1(source_file)
            startline_clone4,endline_clone4 = endline_clone3+startline_clone4,endline_clone3+delta+1

            f_start_line5,f_end_line5 = type3(source_file)
            startline_clone5,endline_clone5 =  startline_clone4+delta+1 , endline_clone4+delta+1+1

    else:
        if more_than2 == 0:
            f_start_line2,f_end_line2 = type2(source_file)
            startline_clone2,endline_clone2 =  startline_clone1+delta+1 , endline_clone1+delta
            
            f_start_line3,f_end_line3 = type3(source_file)
            startline_clone3,endline_clone3 =  startline_clone2+delta , endline_clone2+delta+1

            if more_than2_2(source_file) is True:
                f_start_line4,f_end_line4,startline_clone4,endline_clone4 = type1_2(source_file)
                delta2 = (endline_clone4 - startline_clone4)+1
                startline_clone4,endline_clone4 = endline_clone3+2,endline_clone3+delta2+1

                f_start_line5,f_end_line5 = type3_2(source_file)
                startline_clone5,endline_clone5 = endline_clone4+2,endline_clone4+delta2+1
            else:
                f_start_line4,f_end_line4,startline_clone4,endline_clone4 = type1_2(source_file)
                delta2 = (endline_clone4 - startline_clone4)+1
                startline_clone4,endline_clone4 = endline_clone3+2,endline_clone3+delta2+1

                f_start_line5,f_end_line5 = type3_2(source_file)
                startline_clone5,endline_clone5 = endline_clone4+2,endline_clone4+delta2+1+1

        else:
            f_start_line2,f_end_line2 = type2(source_file)
            startline_clone2,endline_clone2 =  startline_clone1+delta+1 , endline_clone1+delta+1
            
            f_start_line3,f_end_line3 = type3(source_file)
            startline_clone3,endline_clone3 =  startline_clone2+delta+1 , endline_clone2+delta+1+1

            if more_than2_2(source_file) is True:
                f_start_line4,f_end_line4,startline_clone4,endline_clone4 = type1_2(source_file)
                delta2 = (endline_clone4 - startline_clone4)+1
                startline_clone4,endline_clone4 = endline_clone3+2,endline_clone3+delta2+1

                f_start_line5,f_end_line5 = type3_2(source_file)
                startline_clone5,endline_clone5 = endline_clone4+2,endline_clone4+delta2+1
            else:
                f_start_line4,f_end_line4,startline_clone4,endline_clone4 = type1_2(source_file)
                delta2 = (endline_clone4 - startline_clone4)+1
                startline_clone4,endline_clone4 = endline_clone3+2,endline_clone3+delta2+1

                f_start_line5,f_end_line5 = type3_2(source_file)
                startline_clone5,endline_clone5 = endline_clone4+2,endline_clone4+delta2+1+1

                
    json_format = {"source file":source_file ,"clone file": clone_file,\
                             "clone 1":{"clone type":"type1","code fragment":'{ "start line": '+str(f_start_line1)+', "end line": '+str(f_end_line1)+' }',\
                                               "code clone":'{ "start line":'+str(startline_clone1)+', "end line": '+str(endline_clone1)+' }'}\
                            ,"clone 2":{"clone type":"type2","code fragment":'{ "start line": '+str(f_start_line2)+', "end line": '+str(f_end_line2)+' }',\
                                               "code clone":'{ "start line":'+str(startline_clone2)+', "end line": '+str(endline_clone2)+' }'}\
                            ,"clone 3":{"clone type":"type3","code fragment":'{ "start line": '+str(f_start_line3)+', "end line": '+str(f_end_line3)+' }',\
                                               "code clone":'{ "start line":'+str(startline_clone3)+', "end line": '+str(endline_clone3)+' }'}\
                            ,"clone 4":{"clone type":"type1","code fragment":'{ "start line": '+str(f_start_line4)+', "end line": '+str(f_end_line4)+' }',\
                                               "code clone":'{ "start line":'+str(startline_clone4)+', "end line": '+str(endline_clone4)+' }'}\
                            ,"clone 5":{"clone type":"type3","code fragment":'{ "start line": '+str(f_start_line5)+', "end line": '+str(f_end_line5)+' }',\
                                               "code clone":'{ "start line":'+str(startline_clone5)+', "end line": '+str(endline_clone5)+' }'}}

    
    json_str = json.dumps(json_format, indent=4, sort_keys=True)

    with open(reportfile,'w',encoding="utf-8") as f:
        f.write(json_str)

    with open(clone_file, 'a',encoding="utf-8") as f:
        f.write("\n"+ original)

    print ("done")

main(source_file,clone_file)
    
    
